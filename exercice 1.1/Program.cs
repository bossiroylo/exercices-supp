﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercice_1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            //J'attribue des valeurs a mes variables 
            int a = 5;
            int b = 3;
            int c = 0;
            Console.WriteLine(a);
            Console.WriteLine(b);
            //J'inverse les valeurs de mes variables
            c = a;
            a = b;
            b = c;
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.ReadKey();
        }
    }
}
