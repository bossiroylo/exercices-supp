﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercice_1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            //On demande a l'utilisateur de rentrer un nombre entre 1 à 10
            Console.WriteLine("Veuillez entrer un nombre entre 1 à 10 : ");
            //on stock la demande de l'utilisateur dans une variable. Ici on stock la réponse dans "nb"
            string nb =Console.ReadLine();
            //On convertit la réponse qui est en chaine de caractère en un entier
            int nombre = int.Parse(nb);
            //On écrit dans la console la réponse de l'utilisateur multuplier par elle même pour avoir le carrer
            Console.WriteLine(nombre*nombre);
            Console.ReadKey();
        }
    }
}
