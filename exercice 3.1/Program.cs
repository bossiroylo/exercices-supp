﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercice_3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tab = new string[3];
            tab[0] = "riri";
            tab[1] = "fifi";
            tab[2] = "loulou";

            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }

            foreach (string item in tab)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }
}
