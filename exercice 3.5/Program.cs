﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercice_3._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez introduire 5 nombre qu'on va trier");
            List<int> liste = new List<int>();

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Veuillez introduire un nombre");
                string nombre = Console.ReadLine();
                int nb = int.Parse(nombre);
                liste.Add(nb);
            }
            liste.Sort();
            for (int a = 0; a < 5; a++)
            {
                Console.WriteLine(liste[a]);
            }
            Console.ReadKey();
        }
    }
}
