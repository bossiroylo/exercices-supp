﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercice_2._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez entrer 1 premier nombre : ");
            string nombre1 = Console.ReadLine();
            int nb1 = int.Parse(nombre1);
            Console.WriteLine("Veuillez entrer un symbole : +,-,*,/ : ");
            string symbole = Console.ReadLine();
            Console.WriteLine("Veuillez entrer un second nombre : ");
            string nombre2 = Console.ReadLine();
            int nb2 = int.Parse(nombre2);

            if (symbole == "+")
            {
                Console.WriteLine(nb1 + nb2);
            }
            if (symbole == "*")
            {
                Console.WriteLine(nb1 * nb2);
            }
            if (symbole == "-")
            {
                Console.WriteLine(nb1 - nb2);
            }
            if (symbole == "/" & nb2 != 0)
            {
                Console.WriteLine(nb1 / nb2);
            }
            if (symbole == "/" & nb2 == 0)
            {
                Console.WriteLine("on ne peut pas diviser par 0");
            }
                Console.ReadKey();
        }
    }
}
